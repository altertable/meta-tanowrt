#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append = ".tano0"
COMPATIBLE_MACHINE:append = "|am574x-idk"
TARGET:am574x-idk = "am57xx-pru0-prueth-fw.elf am57xx-pru1-prueth-fw.elf"
