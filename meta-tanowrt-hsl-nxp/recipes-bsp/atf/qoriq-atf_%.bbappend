#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#
PR:append:ls1028ardb = ".nxp0"
PLATFORM:ls1028ardb = "ls1028ardb"
