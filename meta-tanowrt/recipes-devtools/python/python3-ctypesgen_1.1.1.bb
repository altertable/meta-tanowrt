#
# SPDX-License-Identifier: MIT
# Copyright (C) 2023, Tano Systems LLC
#
# Author: Anton Kikin <a.kikin@tano-system.com>
#
SUMMARY = "Python wrapper generator for ctypes"
HOMEPAGE = "https://github.com/ctypesgen/ctypesgen"
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2648b0fab622f4743600090ddda451ce"

SRC_URI[sha256sum] = "deaa2d64a95d90196a2e8a689cf9b952be6f3366f81e835245354bf9dbac92f6"

inherit pypi python_setuptools_build_meta

BBCLASSEXTEND = "native nativesdk"
