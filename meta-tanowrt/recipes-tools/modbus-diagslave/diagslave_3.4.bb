#
# SPDX-License-Identifier: MIT
#
# diagslave is a simple command line based Modbus slave
# simulator and test utility. diagslave is using the FieldTalk Modbus driver.
#
# This file Copyright (C) 2019,2024 Anton Kikin <a.kikin@tano-systems.com>
#
PR = "tano0"

SUMMARY = "FieldTalk Modbus Diagnostic Slave Simulator"
HOMEPAGE = "https://www.modbusdriver.com/diagslave.html"
LICENSE = "proconX-LICENSE-FREE"
LIC_FILES_CHKSUM = "file://LICENSE-FREE.txt;md5=60cf88fcfd1f3ebe7d3780ba8bbc53b6"

COMPATIBLE_HOST = "(i.86.*|x86_64.*|arm.*|aarch64.*)-linux.*"

SRC_URI = "https://www.modbusdriver.com/downloads/diagslave.tgz"
SRC_URI[md5sum] = "3af6a11c1381ed452e56995b31989890"
SRC_URI[sha256sum] = "e31f8ea9665a6f2c748e41ddd32988ccad4f2cf9ca5c481562670f2926928107"

S = "${WORKDIR}/diagslave"

INSANE_SKIP:${PN}:append = "already-stripped"

ARCH_DIR:x86-64 = "x86_64-linux-gnu"
ARCH_DIR:i586 = "i686-linux-gnu"
ARCH_DIR:i686 = "i686-linux-gnu"
ARCH_DIR:arm = "arm-linux-gnueabihf"
ARCH_DIR:aarch64 = "aarch64-linux-gnu"

do_install () {
	install -m 0755 -d ${D}${bindir}
	install -m 0755 ${S}/${ARCH_DIR}/diagslave ${D}${bindir}/diagslave
}
