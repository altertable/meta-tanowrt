#
# SPDX-License-Identifier: MIT
#
# Copyright (C) 2004, Advanced Micro Devices, Inc.  All Rights Reserved
# Copyright (C) 2024, Tano Systems LLC, All Rights Reserved
#
# Creates a bootable image using syslinux (for x86), your kernel
# and an optional initrd.
#
# Based on original image-live.bbclass from openembedded-core layer
#

inherit live-vm-common image-artifact-names

do_bootimg[depends] += "dosfstools-native:do_populate_sysroot \
                        mtools-native:do_populate_sysroot \
                        cdrtools-native:do_populate_sysroot \
                        virtual/kernel:do_deploy \
                        ${@bb.utils.contains('MACHINE_FEATURES', 'pcbios', \
                                             d.getVar('MLPREFIX') + 'syslinux:do_populate_sysroot \
                                             syslinux-native:do_populate_sysroot', '', d)} \
                        ${@bb.utils.contains('MACHINE_FEATURES', 'efi', \
                                             bb.utils.contains('MACHINE_FEATURES', 'pcbios', \
                                                               '', 'xorriso-native:do_populate_sysroot', d), '', d)} \
                        ${@'%s:do_image_%s' % (d.getVar('PN'), d.getVar('LIVE_ROOTFS_TYPE').replace('-', '_')) if d.getVar('ROOTFS') else ''} \
                        "

LABELS_LIVE ?= "boot install"
ROOT_LIVE ?= "root=/dev/ram0"
INITRD_IMAGE_LIVE ?= "${MLPREFIX}core-image-minimal-initramfs"
INITRD_LIVE ?= "${DEPLOY_DIR_IMAGE}/${INITRD_IMAGE_LIVE}-${MACHINE}.${INITRAMFS_FSTYPES}"

LIVE_ROOTFS_TYPE ?= "ext4"
ROOTFS ?= "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${LIVE_ROOTFS_TYPE}"

IMAGE_TYPEDEP:tn-iso = "${LIVE_ROOTFS_TYPE}"

python() {
    image_b = d.getVar('IMAGE_BASENAME')
    initrd_i = d.getVar('INITRD_IMAGE_LIVE')
    if image_b == initrd_i:
        bb.error('INITRD_IMAGE_LIVE %s cannot use image tn-iso.' % initrd_i)
        bb.fatal('Check IMAGE_FSTYPES and INITRAMFS_FSTYPES settings.')
    elif initrd_i:
        d.appendVarFlag('do_bootimg', 'depends', ' %s:do_image_complete' % initrd_i)
}

ISODIR = "${S}/iso"
EFIIMGDIR = "${S}/efi_img"
COMPACT_ISODIR = "${S}/iso.z"

ISOLINUXDIR ?= "/isolinux"
ISO_BOOTIMG = "isolinux/isolinux.bin"
ISO_BOOTCAT = "isolinux/boot.cat"
MKISOFS_BOOT_OPTIONS = "-no-emul-boot -boot-load-size 4 -boot-info-table"

BOOTIMG_VOLUME_ID   ?= "boot"
BOOTIMG_EXTRA_SPACE ?= "512"

def compute_chs(sector_z):
    C = int(sector_z / (63 * 255))
    H = int((sector_z % (63 * 255)) / 63)
    # convert zero-based sector to CHS format
    S = int(sector_z % 63) + 1
    # munge accord to partition table format
    S = (S & 0x3f) | (((C >> 8) & 0x3) << 6)
    C = (C & 0xFF)
    return (C, H, S)

def mk_efi_part_table(iso, start, length):
    from struct import pack

    # Compute starting and ending CHS addresses for the partition entry.
    (s_C, s_H, s_S) = compute_chs(start)
    (e_C, e_H, e_S) = compute_chs(start + length - 1)

    # Write the 66 byte partition table to bytes 0x1BE through 0x1FF in
    # sector 0 of the .ISO.
    #
    # See the partition table format here:
    # http://en.wikipedia.org/wiki/Master_boot_record#Sector_layout
    f = open(iso, 'r+b')
    f.seek(0x1BE)
    f.write(pack("<8BLL48xH", 0x80, s_H, s_S, s_C,
                 0xEF, e_H, e_S, e_C, start, length, 0xAA55))
    f.close()

def install_efi_part_table(iso_img):
    import subprocess

    find_efi_img_cmd = "xorriso -indev %s -find /efi.img \
                        -name efi.img -exec report_lba --" % iso_img
    ret = subprocess.run(find_efi_img_cmd.split(), capture_output=True)
    efi_img_start = -1
    efi_img_length = -1
    for line in ret.stdout.decode(encoding='utf-8').split("\n"):
        if "File data lba:" in line and "/efi.img" in line:
            file_stat = line[14:].split(',')
            efi_img_start = int(file_stat[1].strip()) * 4
            efi_img_length = int(int(file_stat[3].strip()) / 512)
            break
    if (efi_img_start < 0) or (efi_img_length < 0):
        bb.fatal("Failed to determine /efi.img attributes")
    mk_efi_part_table(iso_img, efi_img_start, efi_img_length)

populate_live() {
	populate_kernel $1
	if [ -s "${ROOTFS}" ]; then
		install -m 0644 ${ROOTFS} $1/rootfs.img
	fi
}

build_iso_base() {
	# ${INITRD} is a list of multiple filesystem images
	for fs in ${INITRD}
	do
		if [ ! -s "$fs" ]; then
			bbwarn "ISO image will not be created. $fs is invalid."
			return
		fi
	done

	populate_live ${ISODIR}

	if [ "${PCBIOS}" = "1" ]; then
		syslinux_iso_populate ${ISODIR}
	fi
	if [ "${EFI}" = "1" ]; then
		efi_iso_populate ${ISODIR}
		build_fat_img ${EFIIMGDIR} ${ISODIR}/efi.img
	fi

	# We used to have support for zisofs; this is a relic of that
	mkisofs_compress_opts="-r"

	# Check the size of ${ISODIR}/rootfs.img, use mkisofs -iso-level 3
	# when it exceeds 3.8GB, the specification is 4G - 1 bytes, we need
	# leave a few space for other files.
	mkisofs_iso_level=""

	if [ -n "${ROOTFS}" ] && [ -s "${ROOTFS}" ]; then
		rootfs_img_size=`stat -c '%s' ${ISODIR}/rootfs.img`
		# 4080218931 = 3.8 * 1024 * 1024 * 1024
		if [ $rootfs_img_size -gt 4080218931 ]; then
			bbnote "${ISODIR}/rootfs.img execeeds 3.8GB, using '-iso-level 3' for mkisofs"
			mkisofs_iso_level="-iso-level 3"
		fi
	fi

	if [ "${PCBIOS}" = "1" ]; then
		if [ "${EFI}" = "1" ]; then
			# EFI+PCBIOS
			mkisofs -A ${BOOTIMG_VOLUME_ID} -V ${BOOTIMG_VOLUME_ID} \
				-o ${IMGDEPLOYDIR}/${IMAGE_NAME}.iso \
				-b ${ISO_BOOTIMG} -c ${ISO_BOOTCAT} \
				$mkisofs_compress_opts ${MKISOFS_BOOT_OPTIONS} $mkisofs_iso_level \
				-eltorito-alt-boot -eltorito-platform efi \
				-b efi.img -no-emul-boot \
				${ISODIR}
			isohybrid_args="-u"
		else
			# PCBIOS only
			mkisofs -V ${BOOTIMG_VOLUME_ID} \
				-o ${IMGDEPLOYDIR}/${IMAGE_NAME}.iso \
				-b ${ISO_BOOTIMG} -c ${ISO_BOOTCAT} \
				$mkisofs_compress_opts \
				${MKISOFS_BOOT_OPTIONS} $mkisofs_iso_level ${ISODIR}
		fi

		isohybrid $isohybrid_args ${IMGDEPLOYDIR}/${IMAGE_NAME}.iso
	else
		mkisofs -A ${BOOTIMG_VOLUME_ID} -V ${BOOTIMG_VOLUME_ID} \
			-o ${IMGDEPLOYDIR}/${IMAGE_NAME}.iso \
			$mkisofs_compress_opts $mkisofs_iso_level \
			${ISODIR}
	fi
}

python build_iso() {
    # Only create an ISO if we have an INITRD and the live or iso image type was selected
    if not d.getVar('INITRD') or \
       not bb.utils.contains_any('IMAGE_FSTYPES', 'tn-iso', True, False, d):
        bb.note("ISO image will not be created.")
        return
    bb.build.exec_func("build_iso_base", d)
    if d.getVar("PCBIOS") != "1" and d.getVar("EFI") == "1":
        install_efi_part_table(d.getVar("IMGDEPLOYDIR") + "/" + \
                               d.getVar("IMAGE_NAME") + ".iso")
}

build_fat_img() {
	FATSOURCEDIR=$1
	FATIMG=$2

	# Calculate the size required for the final image including the
	# data and filesystem overhead.
	# Sectors: 512 bytes
	#  Blocks: 1024 bytes

	# Determine the sector count just for the data
	SECTORS=$(expr $(du --apparent-size -ks ${FATSOURCEDIR} | cut -f 1) \* 2)

	# Account for the filesystem overhead. This includes directory
	# entries in the clusters as well as the FAT itself.
	# Assumptions:
	#   FAT32 (12 or 16 may be selected by mkdosfs, but the extra
	#   padding will be minimal on those smaller images and not
	#   worth the logic here to caclulate the smaller FAT sizes)
	#   < 16 entries per directory
	#   8.3 filenames only

	# 32 bytes per dir entry
	DIR_BYTES=$(expr $(find ${FATSOURCEDIR} | tail -n +2 | wc -l) \* 32)
	# 32 bytes for every end-of-directory dir entry
	DIR_BYTES=$(expr $DIR_BYTES + $(expr $(find ${FATSOURCEDIR} -type d | tail -n +2 | wc -l) \* 32))
	# 4 bytes per FAT entry per sector of data
	FAT_BYTES=$(expr $SECTORS \* 4)
	# 4 bytes per FAT entry per end-of-cluster list
	FAT_BYTES=$(expr $FAT_BYTES + $(expr $(find ${FATSOURCEDIR} -type d | tail -n +2 | wc -l) \* 4))

	# Use a ceiling function to determine FS overhead in sectors
	DIR_SECTORS=$(expr $(expr $DIR_BYTES + 511) / 512)
	# There are two FATs on the image
	FAT_SECTORS=$(expr $(expr $(expr $FAT_BYTES + 511) / 512) \* 2)
	SECTORS=$(expr $SECTORS + $(expr $DIR_SECTORS + $FAT_SECTORS))

	# Determine the final size in blocks accounting for some padding
	BLOCKS=$(expr $(expr $SECTORS / 2) + ${BOOTIMG_EXTRA_SPACE})

	# mkdosfs will sometimes use FAT16 when it is not appropriate,
	# resulting in a boot failure from SYSLINUX. Use FAT32 for
	# images larger than 512MB, otherwise let mkdosfs decide.
	if [ $(expr $BLOCKS / 1024) -gt 512 ]; then
		FATSIZE="-F 32"
	fi

	# mkdosfs will fail if ${FATIMG} exists. Since we are creating an
	# new image, it is safe to delete any previous image.
	if [ -e ${FATIMG} ]; then
		rm ${FATIMG}
	fi

	if [ -z "${HDDIMG_ID}" ]; then
		mkdosfs ${FATSIZE} -n ${BOOTIMG_VOLUME_ID} ${MKDOSFS_EXTRAOPTS} -C ${FATIMG} \
			${BLOCKS}
	else
		mkdosfs ${FATSIZE} -n ${BOOTIMG_VOLUME_ID} ${MKDOSFS_EXTRAOPTS} -C ${FATIMG} \
		${BLOCKS} -i ${HDDIMG_ID}
	fi

	# Copy FATSOURCEDIR recursively into the image file directly
	mcopy -i ${FATIMG} -s ${FATSOURCEDIR}/* ::/
}

python do_bootimg() {
    set_live_vm_vars(d, 'LIVE')
    if d.getVar("PCBIOS") == "1":
        bb.build.exec_func('build_syslinux_cfg', d)
    if d.getVar("EFI") == "1":
        bb.build.exec_func('build_efi_cfg', d)
    bb.build.exec_func('build_iso', d)
    bb.build.exec_func('create_symlinks', d)
}
do_bootimg[subimages] = "iso"
do_bootimg[imgsuffix] = "."

addtask bootimg before do_image_complete after do_rootfs
