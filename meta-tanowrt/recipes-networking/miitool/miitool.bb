#
# SPDX-License-Identifier: MIT
# Copyright (c) 2023 Tano Systems LLC. All rights reserved.
#
SUMMARY = "Tool to read and write registers of a PHY attached to a network device"
HOMEPAGE = "https://github.com/kontron/miitool"
SECTION = "net"

PV = "1.0.0+git${SRCPV}"
PR = "tano0"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://COPYING;md5=246b2349af6ac87c12565940a25b124f"

SRC_URI = "git://github.com/kontron/miitool.git;branch=master;protocol=https"
SRCREV = "b019bc463bf7fc7029d9237982762b92265024f6"

S = "${WORKDIR}/git"

do_install() {
	oe_runmake DESTDIR=${D} install
}
