#
# SPDX-License-Identifier: MIT
# Copyright (c) 2023 Tano Systems LLC. All rights reserved.
#

SUMMARY = "simple dynamic multicast routing daemon that only uses IGMP signalling"
HOMEPAGE = "http://sourceforge.net/projects/igmpproxy/"

PR = "tano0"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=432040ff3a55670c1dec0c32b209ad69"

SRC_URI = "git://github.com/pali/igmpproxy.git;branch=master;protocol=https"
SRCREV = "9762cf8b756a0ddf21f18298f0fb1c52c5fbbfca"

S = "${WORKDIR}/git"

UPSTREAM_CHECK_URI = "https://github.com/pali/${BPN}/releases"

inherit autotools pkgconfig

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

SRC_URI += "\
	file://igmpproxy.config \
	file://igmpproxy.init \
"

do_install:append() {
	install -m 0644 -D ${WORKDIR}/igmpproxy.config ${D}${sysconfdir}/config/igmpproxy
	install -m 0755 -D ${WORKDIR}/igmpproxy.init ${D}${sysconfdir}/init.d/igmpproxy
}

inherit tanowrt-services

TANOWRT_SERVICE_PACKAGES = "igmpproxy"
TANOWRT_SERVICE_SCRIPTS_igmpproxy += "igmpproxy"
TANOWRT_SERVICE_STATE_igmpproxy-igmpproxy ?= "disabled"

CONFFILES:${PN} += "${sysconfdir}/config/igmpproxy"
