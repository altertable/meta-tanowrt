#
# SPDX-License-Identifier: MIT
# Copyright (C) 2023, Tano Systems LLC. All rights reserved.
#

SUMMARY = "CH347 linux driver for I2C, SPI and GPIO modes"
DESCRIPTION = "${SUMMARY}"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

PV = "1.0.0+git${SRCPV}"
PR = "tano7"

SRC_URI = "git://github.com/aystarik/ch347_vcp.git;branch=main;protocol=https"
SRCREV = "08a0aecd7fac42ef7a46c620332cd2225354ab0f"
S = "${WORKDIR}/git"

SRC_URI += "\
	file://0001-Do-not-hardcode-RX-TX-buffer-sizes.patch \
	file://0002-Add-extra-error-messages-to-ch347_data_xfer.patch \
"

inherit tanowrt-kernel-module

do_configure[depends] += "virtual/kernel:do_shared_workdir"

RPROVIDES:${PN} += "kernel-module-mfd-ch347 \
                    kernel-module-i2c-ch347 \
                    kernel-module-spi-ch347 \
                    kernel-module-gpio-ch347"
