#
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Tano Systems LLC. All rights reserved.
#

PR:append = ".tano0"

# Compile always with GCC
TOOLCHAIN = "gcc"
