#
# SPDX-License-Identifier: MIT
# Copyright (c) 2018-2020 Tano Systems LLC. All rights reserved.
#

PR:append = ".tano0"

# Install base-files first
RDEPENDS:${PN} += "base-files"
