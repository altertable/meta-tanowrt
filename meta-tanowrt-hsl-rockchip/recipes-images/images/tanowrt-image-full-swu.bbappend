#
# SPDX-License-Identifier: MIT
# Copyright (c) 2022 Tano Systems LLC. All rights reserved.
#
PR:append = ".rk0"

COMPATIBLE_MACHINE = "boardcon-em356x|rock-pi-s"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/:"
