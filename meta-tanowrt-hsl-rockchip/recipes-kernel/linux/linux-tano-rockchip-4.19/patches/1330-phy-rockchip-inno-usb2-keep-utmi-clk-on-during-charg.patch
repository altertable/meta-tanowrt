From d76dfc01258c2a2cc0ee574c15cbfd217aeecc11 Mon Sep 17 00:00:00 2001
From: William Wu <william.wu@rock-chips.com>
Date: Tue, 20 Jun 2023 14:03:43 +0300
Subject: [PATCH 103/104] phy: rockchip: inno-usb2: keep utmi clk on during
 charge detection

The utmi clk is provided by the USB PHY for the USB controller.
And the utmi clk is disabled if the USB PHY enter suspend mode.
The current charge detection sets the USB PHY in suspend mode
at first, then take about hundreds of milliseconds to do charge
detection, in other words, the utmi clk will be disabled hundreds
of milliseconds. It may cause the USB controller work abnormally
during the charge detection.

Actually, the conditions for charger detection is:
1. Set the utmi_opmode in non-driving mode.
2. Set the utmi_xcvrselect to FS speed.
3. Set the utmi_termselect to FS speed.
4. Enable the DP/DM pulldown resistor.

Signed-off-by: William Wu <william.wu@rock-chips.com>
Change-Id: I1cbf565d5145bdae5bc91132bc5fbff23a5cc443
[Update for kernel 4.19]
Signed-off-by: Anton Kikin <a.kikin@tano-systems.com>
---
 drivers/phy/rockchip/phy-rockchip-inno-usb2.c | 56 ++++++++++++++-----
 drivers/usb/dwc2/gadget.c                     | 10 +++-
 include/linux/phy/phy.h                       |  1 +
 3 files changed, 49 insertions(+), 18 deletions(-)

diff --git a/drivers/phy/rockchip/phy-rockchip-inno-usb2.c b/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
index 48a375928844..1e698ff9c627 100644
--- a/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
+++ b/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
@@ -247,6 +247,7 @@ struct rockchip_usb2phy_cfg {
  * @event_nb: hold event notification callback.
  * @state: define OTG enumeration states before device reset.
  * @mode: the dr_mode of the controller.
+ * @linestate: the usb linestate controlled by software.
  */
 struct rockchip_usb2phy_port {
 	struct phy	*phy;
@@ -275,6 +276,7 @@ struct rockchip_usb2phy_port {
 	struct wake_lock	wakelock;
 	enum usb_otg_state	state;
 	enum usb_dr_mode	mode;
+	enum usb_linestate	linestate;
 };
 
 /**
@@ -720,8 +722,13 @@ static int rockchip_usb2phy_power_on(struct phy *phy)
 
 	mutex_lock(&rport->mutex);
 
-	if (!rport->suspended) {
+	if (!rport->suspended ||
+	    ((rport->port_id == USB2PHY_PORT_OTG) &&
+	     (rport->linestate != PHY_SET_USB_NONE))) {
 		ret = 0;
+		if (rport->linestate == PHY_SET_USB_SUSPEND)
+			ret = property_enable(base, &rport->port_cfg->phy_sus,
+					      false);
 		goto unlock;
 	}
 
@@ -760,7 +767,9 @@ static int rockchip_usb2phy_power_off(struct phy *phy)
 
 	mutex_lock(&rport->mutex);
 
-	if (rport->suspended) {
+	if (rport->suspended ||
+	    ((rport->port_id == USB2PHY_PORT_OTG) &&
+	     (rport->linestate != PHY_SET_USB_NONE))) {
 		ret = 0;
 		goto unlock;
 	}
@@ -878,6 +887,8 @@ static void rockchip_usb2phy_set_linestate(struct phy *phy, enum usb_linestate s
 
 	dev_dbg(&rport->phy->dev, "set linestate %d\n", set_linestate);
 
+	rport->linestate = set_linestate;
+
 	switch (set_linestate) {
 	case PHY_SET_USB_DP_H_DM_L:
 		/*
@@ -930,6 +941,13 @@ static void rockchip_usb2phy_set_linestate(struct phy *phy, enum usb_linestate s
 		property_enable(base, &rport->port_cfg->phy_fs_enc_dis,
 				true);
 		break;
+	case PHY_SET_USB_SUSPEND:
+		/* Disable software to own the fs transceiver */
+		if (rport->port_id == USB2PHY_PORT_OTG)
+			property_enable(base, &rport->port_cfg->phy_fs_xver_en,
+					false);
+		property_enable(base, &rport->port_cfg->phy_sus, true);
+		break;
 	default:
 		break;
 	}
@@ -1124,6 +1142,7 @@ static void rockchip_usb2phy_otg_sm_work(struct work_struct *work)
 		    extcon_get_state(rphy->edev, EXTCON_USB_VBUS_EN) > 0 ) {
 			dev_dbg(&rport->phy->dev, "usb otg host connect\n");
 			rport->state = OTG_STATE_A_HOST;
+			rport->linestate = PHY_SET_USB_NONE;
 			rphy->chg_state = USB_CHG_STATE_UNDEFINED;
 			rphy->chg_type = POWER_SUPPLY_TYPE_UNKNOWN;
 			mutex_unlock(&rport->mutex);
@@ -1175,6 +1194,7 @@ static void rockchip_usb2phy_otg_sm_work(struct work_struct *work)
 		} else {
 			rphy->chg_state = USB_CHG_STATE_UNDEFINED;
 			rphy->chg_type = POWER_SUPPLY_TYPE_UNKNOWN;
+			rport->linestate = PHY_SET_USB_NONE;
 			mutex_unlock(&rport->mutex);
 			rockchip_usb2phy_power_off(rport->phy);
 			mutex_lock(&rport->mutex);
@@ -1191,12 +1211,14 @@ static void rockchip_usb2phy_otg_sm_work(struct work_struct *work)
 			rphy->chg_state = USB_CHG_STATE_UNDEFINED;
 			rphy->chg_type = POWER_SUPPLY_TYPE_UNKNOWN;
 			rport->perip_connected = false;
+			rport->linestate = PHY_SET_USB_NONE;
 			sch_work = false;
 			wake_unlock(&rport->wakelock);
 		} else if (!rport->vbus_attached) {
 			dev_dbg(&rport->phy->dev, "usb disconnect\n");
 			rport->state = OTG_STATE_B_IDLE;
 			rport->perip_connected = false;
+			rport->linestate = PHY_SET_USB_NONE;
 			rphy->chg_state = USB_CHG_STATE_UNDEFINED;
 			rphy->chg_type = POWER_SUPPLY_TYPE_UNKNOWN;
 			delay = OTG_SCHEDULE_DELAY;
@@ -1410,17 +1432,19 @@ static void rockchip_chg_detect_work(struct work_struct *work)
 		rphy->chg_state = USB_CHG_STATE_DETECTED;
 		/* fall through */
 	case USB_CHG_STATE_DETECTED:
-		/* Restore the PHY suspend configuration */
-		phy_sus_reg = &rport->port_cfg->phy_sus;
-		mask = GENMASK(phy_sus_reg->bitend, phy_sus_reg->bitstart);
-		ret = regmap_write(base, phy_sus_reg->offset,
-				   ((rphy->phy_sus_cfg <<
-				     phy_sus_reg->bitstart) |
-				    (mask << BIT_WRITEABLE_SHIFT)));
-		if (ret)
-			dev_err(&rport->phy->dev,
-				"Fail to set phy_sus reg offset 0x%x, ret %d\n",
-				phy_sus_reg->offset, ret);
+		if (rport->linestate == PHY_SET_USB_NONE) {
+			/* Restore the PHY suspend configuration */
+			phy_sus_reg = &rport->port_cfg->phy_sus;
+			mask = GENMASK(phy_sus_reg->bitend, phy_sus_reg->bitstart);
+			ret = regmap_write(base, phy_sus_reg->offset,
+					   ((rphy->phy_sus_cfg <<
+					     phy_sus_reg->bitstart) |
+					    (mask << BIT_WRITEABLE_SHIFT)));
+			if (ret)
+				dev_err(&rport->phy->dev,
+					"Fail to set phy_sus reg offset 0x%x, ret %d\n",
+					phy_sus_reg->offset, ret);
+		}
 		mutex_unlock(&rport->mutex);
 		rockchip_usb2phy_otg_sm_work(&rport->otg_sm_work.work);
 		dev_info(&rport->phy->dev, "charger = %s\n",
@@ -1821,6 +1845,7 @@ static int rockchip_usb2phy_host_port_init(struct rockchip_usb2phy *rphy,
 
 	rport->port_id = USB2PHY_PORT_HOST;
 	rport->port_cfg = &rphy->phy_cfg->port_cfgs[USB2PHY_PORT_HOST];
+	rport->linestate = PHY_SET_USB_NONE;
 
 	/* enter lower power state when suspend */
 	rport->low_power_en =
@@ -1874,6 +1899,7 @@ static int rockchip_usb2phy_otg_port_init(struct rockchip_usb2phy *rphy,
 	rport->vbus_enabled = false;
 	rport->perip_connected = false;
 	rport->prev_iddig = true;
+	rport->linestate = PHY_SET_USB_NONE;
 
 	mutex_init(&rport->mutex);
 
@@ -2838,10 +2864,10 @@ static const struct rockchip_usb2phy_cfg rk3308_phy_cfgs[] = {
 		.reg = 0x100,
 		.num_ports	= 2,
 		.phy_tuning	= rk3308_usb2phy_tuning,
-		.clkout_ctl	= { 0x0108, 4, 4, 0, 0 },
+		.clkout_ctl	= { 0x0108, 4, 4, 1, 0 },
 		.port_cfgs	= {
 			[USB2PHY_PORT_OTG] = {
-				.phy_sus	= { 0x0100, 8, 0, 0, 0x000 },
+				.phy_sus	= { 0x0100, 8, 0, 0, 0x1d1 },
 				.phy_pd_en	= { 0x0100, 8, 0, 0, 0x003 },
 				.phy_fs_xver_en = { 0x010c, 11, 0, 0x019, 0x159 },
 				.bvalid_det_en	= { 0x3020, 2, 2, 0, 1 },
diff --git a/drivers/usb/dwc2/gadget.c b/drivers/usb/dwc2/gadget.c
index 2f83fdd24365..4382d9ab96eb 100644
--- a/drivers/usb/dwc2/gadget.c
+++ b/drivers/usb/dwc2/gadget.c
@@ -4769,6 +4769,9 @@ static enum hrtimer_restart dwc2_check_linestate_timer_fn(struct hrtimer *t)
 			} else {
 				dev_dbg(hsotg->dev, "rcv se0 at retry %d delay %ld\n",
 					retry, wait_conn_delay);
+				phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
+				hsotg->set_linestate = PHY_SET_USB_NONE;
+				delay = ktime_set(0, DWC2_HRTIMER_POLL);
 			}
 		} else {
 			dev_dbg(hsotg->dev, "retry timeout at delay %ld\n",
@@ -4803,12 +4806,13 @@ static enum hrtimer_restart dwc2_check_linestate_timer_fn(struct hrtimer *t)
 					wait_conn_delay);
 				delay = ktime_set(0, DWC2_HRTIMER_WAIT_SE0);
 				goto restart;
+			} else {
+				phy_set_linestate(hsotg->phy, PHY_SET_USB_SUSPEND);
+				hsotg->set_linestate = PHY_SET_USB_NONE;
+				delay = ktime_set(0, DWC2_HRTIMER_POLL);
 			}
 		}
 
-		phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
-		hsotg->set_linestate = PHY_SET_USB_NONE;
-		delay = ktime_set(0, DWC2_HRTIMER_POLL);
 		break;
 	default:
 		delay = ktime_set(0, DWC2_HRTIMER_POLL);
diff --git a/include/linux/phy/phy.h b/include/linux/phy/phy.h
index da5ced68c185..ec6cf19bc0b9 100644
--- a/include/linux/phy/phy.h
+++ b/include/linux/phy/phy.h
@@ -61,6 +61,7 @@ enum usb_linestate {
 	PHY_SET_USB_CONNECT,
 	PHY_SET_USB_DONE,
 	PHY_SET_USB_FS_ENC_DIS,
+	PHY_SET_USB_SUSPEND,
 };
 
 /**
-- 
2.17.1

