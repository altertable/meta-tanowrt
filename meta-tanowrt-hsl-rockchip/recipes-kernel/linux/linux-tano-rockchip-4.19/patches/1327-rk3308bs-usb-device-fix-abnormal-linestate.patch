From 20ebfb3669f0a1fd3b40fa4701b4b3750b23a707 Mon Sep 17 00:00:00 2001
From: William Wu <william.wu@rock-chips.com>
Date: Tue, 20 Jun 2023 13:22:15 +0300
Subject: [PATCH] rk3308bs: usb: device: fix abnormal linestate

Change-Id: Ifc18c44bc5fcc4a8ba223c4a6be9da7bee4db20b
Signed-off-by: William Wu <william.wu@rock-chips.com>
[Update for kernel 4.19]
Signed-off-by: Anton Kikin <a.kikin@tano-systems.com>
---
 drivers/phy/phy-core.c                        |  22 +++
 drivers/phy/rockchip/phy-rockchip-inno-usb2.c |  82 +++++++-
 drivers/usb/dwc2/core.h                       |   6 +
 drivers/usb/dwc2/gadget.c                     | 175 +++++++++++++++++-
 include/linux/phy/phy.h                       |  21 ++
 5 files changed, 302 insertions(+), 4 deletions(-)

diff --git a/drivers/phy/phy-core.c b/drivers/phy/phy-core.c
index b7b041c5a5c1..d1ec0ac540ae 100644
--- a/drivers/phy/phy-core.c
+++ b/drivers/phy/phy-core.c
@@ -377,6 +377,28 @@ int phy_set_mode(struct phy *phy, enum phy_mode mode)
 }
 EXPORT_SYMBOL_GPL(phy_set_mode);
 
+void phy_set_linestate(struct phy *phy, enum usb_linestate linestate)
+{
+	if (!phy || !phy->ops->set_linestate)
+		return;
+
+	phy->ops->set_linestate(phy, linestate);
+}
+EXPORT_SYMBOL_GPL(phy_set_linestate);
+
+int phy_get_linestate(struct phy *phy)
+{
+	int ret;
+
+	if (!phy || !phy->ops->get_linestate)
+		return 0;
+
+	ret = phy->ops->get_linestate(phy);
+
+	return ret;
+}
+EXPORT_SYMBOL_GPL(phy_get_linestate);
+
 int phy_reset(struct phy *phy)
 {
 	int ret;
diff --git a/drivers/phy/rockchip/phy-rockchip-inno-usb2.c b/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
index d213ddf644ef..bb94d60591d5 100644
--- a/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
+++ b/drivers/phy/rockchip/phy-rockchip-inno-usb2.c
@@ -130,6 +130,8 @@ struct rockchip_chg_det_reg {
 /**
  * struct rockchip_usb2phy_port_cfg: usb-phy port configuration.
  * @phy_sus: phy suspend register.
+ * @phy_pd_en; usb dp/dm 45ohm pulldown enable register.
+ * @phy_fs_xver_en: fs transceiver own and data bit enable register.
  * @bvalid_det_en: vbus valid rise detection enable register.
  * @bvalid_det_st: vbus valid rise detection status register.
  * @bvalid_det_clr: vbus valid rise detection clear register.
@@ -161,6 +163,8 @@ struct rockchip_chg_det_reg {
  */
 struct rockchip_usb2phy_port_cfg {
 	struct usb2phy_reg	phy_sus;
+	struct usb2phy_reg	phy_pd_en;
+	struct usb2phy_reg	phy_fs_xver_en;
 	struct usb2phy_reg	bvalid_det_en;
 	struct usb2phy_reg	bvalid_det_st;
 	struct usb2phy_reg	bvalid_det_clr;
@@ -863,12 +867,84 @@ static int rockchip_usb2phy_set_mode(struct phy *phy, enum phy_mode mode)
 	return ret;
 }
 
+static void rockchip_usb2phy_set_linestate(struct phy *phy, enum usb_linestate set_linestate)
+{
+	struct rockchip_usb2phy_port *rport = phy_get_drvdata(phy);
+	struct rockchip_usb2phy *rphy = dev_get_drvdata(phy->dev.parent);
+	struct regmap *base = get_reg_base(rphy);
+
+	if (rport->port_id != USB2PHY_PORT_OTG)
+		return;
+
+	dev_dbg(&rport->phy->dev, "set linestate %d\n", set_linestate);
+
+	switch (set_linestate) {
+	case PHY_SET_USB_DP_H_DM_L:
+		/*
+		 * Set DP HIGH and DM LOW
+		 * For Low-speed, it's Resume state:
+		 * Driver low speed differential "1" (data K state) to resume state,
+		 * and later, we can set PHY in PHY_SET_USB_DISC, it aims to make a
+		 * disconnect condition for low speed: Resume state -> SE0.
+		 */
+		property_enable(base, &rport->port_cfg->phy_pd_en, false);
+		property_enable(base, &rport->port_cfg->phy_fs_xver_en, true);
+		break;
+	case PHY_SET_USB_DISC:
+		/*
+		 * SE0 state:
+		 * Remove 1.5K DP pullup and enable 45ohm pulldown on DP/DM to
+		 * force DP/DM Voltage to low level to force Host detect disconnect.
+		 */
+		property_enable(base, &rport->port_cfg->phy_pd_en, true);
+
+		/*
+		 * Prepare software to own the fs transceiver and driver full speed
+		 * differential "1", aka, DP high and DM low.
+		 */
+		property_enable(base, &rport->port_cfg->phy_fs_xver_en, true);
+		break;
+	case PHY_SET_USB_CONNECT:
+		/*
+		 * Full speed Connect (IDLE) state:
+		 * Now we have make sure that Host can detect the disconnect with
+		 * DP/DM 45ohm pulldown, then we can remove DP/DM 45ohm pulldown,
+		 * and DP becomes high, DM becomes low to wait for connection.
+		 */
+		property_enable(base, &rport->port_cfg->phy_pd_en, false);
+		break;
+	case PHY_SET_USB_DONE:
+		/* Remove DP/DM 45ohm pulldown */
+		property_enable(base, &rport->port_cfg->phy_pd_en, false);
+
+		/* Disable software to own the fs transceiver and let's go */
+		property_enable(base, &rport->port_cfg->phy_fs_xver_en, false);
+		break;
+	default:
+		break;
+	}
+}
+
+static int rockchip_usb2phy_get_linestate(struct phy *phy)
+{
+	struct rockchip_usb2phy_port *rport = phy_get_drvdata(phy);
+	struct rockchip_usb2phy *rphy = dev_get_drvdata(phy->dev.parent);
+	unsigned int ul, ul_mask;
+
+	regmap_read(rphy->grf, rport->port_cfg->utmi_ls.offset, &ul);
+	ul_mask = GENMASK(rport->port_cfg->utmi_ls.bitend,
+			  rport->port_cfg->utmi_ls.bitstart);
+	return ((ul & ul_mask) >> rport->port_cfg->utmi_ls.bitstart);
+}
+
 static const struct phy_ops rockchip_usb2phy_ops = {
 	.init		= rockchip_usb2phy_init,
 	.exit		= rockchip_usb2phy_exit,
 	.power_on	= rockchip_usb2phy_power_on,
 	.power_off	= rockchip_usb2phy_power_off,
 	.set_mode	= rockchip_usb2phy_set_mode,
+	.set_linestate	= rockchip_usb2phy_set_linestate,
+	.get_linestate	= rockchip_usb2phy_get_linestate,
 	.owner		= THIS_MODULE,
 };
 
@@ -2741,10 +2817,12 @@ static const struct rockchip_usb2phy_cfg rk3308_phy_cfgs[] = {
 		.reg = 0x100,
 		.num_ports	= 2,
 		.phy_tuning	= rk3308_usb2phy_tuning,
-		.clkout_ctl	= { 0x0108, 4, 4, 1, 0 },
+		.clkout_ctl	= { 0x0108, 4, 4, 0, 0 },
 		.port_cfgs	= {
 			[USB2PHY_PORT_OTG] = {
-				.phy_sus	= { 0x0100, 8, 0, 0, 0x1d1 },
+				.phy_sus	= { 0x0100, 8, 0, 0, 0x000 },
+				.phy_pd_en	= { 0x0100, 8, 0, 0, 0x003 },
+				.phy_fs_xver_en = { 0x010c, 11, 0, 0x019, 0x159 },
 				.bvalid_det_en	= { 0x3020, 2, 2, 0, 1 },
 				.bvalid_det_st	= { 0x3024, 2, 2, 0, 1 },
 				.bvalid_det_clr = { 0x3028, 2, 2, 0, 1 },
diff --git a/drivers/usb/dwc2/core.h b/drivers/usb/dwc2/core.h
index cc5f20c433b1..f41011fdfeb6 100644
--- a/drivers/usb/dwc2/core.h
+++ b/drivers/usb/dwc2/core.h
@@ -833,6 +833,8 @@ struct dwc2_hregs_backup {
  *                      - USB_DR_MODE_PERIPHERAL
  *                      - USB_DR_MODE_HOST
  *                      - USB_DR_MODE_OTG
+ * @set_linestate:	set usb dp/dm linestate.
+ * @wait_enum_count	The count used for check_linestate_timer to wait enumeration done.
  * @hcd_enabled:	Host mode sub-driver initialization indicator.
  * @gadget_enabled:	Peripheral mode sub-driver initialization indicator.
  * @ll_hw_enabled:	Status of low-level hardware resources.
@@ -859,6 +861,7 @@ struct dwc2_hregs_backup {
  * @wf_otg:             Work object for handling Connector ID Status Change
  *                      interrupt
  * @wkp_timer:          Timer object for handling Wakeup Detected interrupt
+ * @check_linestate_timer Timer used to check usb dp/dm linestate.
  * @lx_state:           Lx state of connected device
  * @gr_backup: Backup of global registers during suspend
  * @dr_backup: Backup of device registers during suspend
@@ -1020,11 +1023,13 @@ struct dwc2_hsotg {
 	struct dwc2_core_params params;
 	enum usb_otg_state op_state;
 	enum usb_dr_mode dr_mode;
+	enum usb_linestate set_linestate;
 	unsigned int hcd_enabled:1;
 	unsigned int gadget_enabled:1;
 	unsigned int ll_hw_enabled:1;
 	unsigned int ll_phy_enabled:1;
 	unsigned int hibernated:1;
+	u8 wait_enum_count;
 	bool bus_suspended;
 	u16 frame_number;
 
@@ -1049,6 +1054,7 @@ struct dwc2_hsotg {
 	struct workqueue_struct *wq_otg;
 	struct work_struct wf_otg;
 	struct timer_list wkp_timer;
+	struct hrtimer check_linestate_timer;
 	enum dwc2_lx_state lx_state;
 	struct dwc2_gregs_backup gr_backup;
 	struct dwc2_dregs_backup dr_backup;
diff --git a/drivers/usb/dwc2/gadget.c b/drivers/usb/dwc2/gadget.c
index a12a5fb655e5..07ed0ba0b753 100644
--- a/drivers/usb/dwc2/gadget.c
+++ b/drivers/usb/dwc2/gadget.c
@@ -28,6 +28,7 @@
 #include <linux/usb/ch9.h>
 #include <linux/usb/gadget.h>
 #include <linux/usb/phy.h>
+#include <linux/rockchip/cpu.h>
 
 #include "core.h"
 #include "hw.h"
@@ -3078,8 +3079,17 @@ static void dwc2_hsotg_irq_enumdone(struct dwc2_hsotg *hsotg)
 		 */
 		break;
 	}
-	dev_info(hsotg->dev, "new device is %s\n",
-		 usb_speed_string(hsotg->gadget.speed));
+
+	if ((hsotg->set_linestate != PHY_SET_USB_NONE) &&
+	    (hsotg->gadget.speed == USB_SPEED_FULL)) {
+		dev_dbg(hsotg->dev, "EnumDone FS in linestate %d\n",
+			hsotg->set_linestate);
+		hsotg->gadget.speed = USB_SPEED_UNKNOWN;
+		goto out;
+	} else {
+		dev_dbg(hsotg->dev, "new device is %s\n",
+			 usb_speed_string(hsotg->gadget.speed));
+	}
 
 	/*
 	 * we should now know the maximum packet size for an
@@ -3105,6 +3115,7 @@ static void dwc2_hsotg_irq_enumdone(struct dwc2_hsotg *hsotg)
 
 	dwc2_hsotg_enqueue_setup(hsotg);
 
+out:
 	dev_dbg(hsotg->dev, "EP0: DIEPCTL0=0x%08x, DOEPCTL0=0x%08x\n",
 		dwc2_readl(hsotg, DIEPCTL0),
 		dwc2_readl(hsotg, DOEPCTL0));
@@ -3156,6 +3167,7 @@ void dwc2_hsotg_disconnect(struct dwc2_hsotg *hsotg)
 
 	hsotg->connected = 0;
 	hsotg->test_mode = 0;
+	hsotg->gadget.speed = USB_SPEED_UNKNOWN;
 
 	/* all endpoints should be shutdown */
 	for (ep = 0; ep < hsotg->num_of_eps; ep++) {
@@ -3430,8 +3442,20 @@ static void dwc2_hsotg_core_disconnect(struct dwc2_hsotg *hsotg)
 
 void dwc2_hsotg_core_connect(struct dwc2_hsotg *hsotg)
 {
+	ktime_t delay;
+
 	/* remove the soft-disconnect and let's go */
 	dwc2_clear_bit(hsotg, DCTL, DCTL_SFTDISCON);
+
+	if (soc_is_rk3308bs()) {
+		/* Disable software to own the fs transceiver */
+		phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
+		hsotg->set_linestate = PHY_SET_USB_NONE;
+		hsotg->wait_enum_count = 0;
+		delay = ktime_set(0, 5 * NSEC_PER_MSEC);
+		hrtimer_start(&hsotg->check_linestate_timer, delay,
+			      HRTIMER_MODE_REL);
+	}
 }
 
 /**
@@ -4657,6 +4681,146 @@ static int dwc2_hsotg_hw_cfg(struct dwc2_hsotg *hsotg)
 	return 0;
 }
 
+#define DM_IS_HIGH			0x2
+#define DP_DM_IS_HIGH			0x3
+#define RETRY_CNT			20
+#define DWC2_HRTIMER_WAIT_SE0		(5 * NSEC_PER_MSEC)
+#define DWC2_HRTIMER_LS_RESUME		(10 * NSEC_PER_MSEC)
+#define DWC2_HRTIMER_DIS		(15 * NSEC_PER_MSEC)
+#define DWC2_HRTIMER_FORCE_DP_H(ms)	(ms * NSEC_PER_MSEC)
+#define DWC2_HRTIMER_POLL		(2000 * NSEC_PER_MSEC)
+
+static enum hrtimer_restart dwc2_check_linestate_timer_fn(struct hrtimer *t)
+{
+	enum hrtimer_restart ret;
+	struct dwc2_hsotg *hsotg = container_of(t, struct dwc2_hsotg, check_linestate_timer);
+	int linestate, enum_speed, max_speed;
+	ktime_t delay;
+	u32 dsts, dcfg, dctl;
+	static int retry = RETRY_CNT;
+	static unsigned long wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(100);
+
+	if (!dwc2_is_device_mode(hsotg)) {
+		dev_dbg(hsotg->dev, "currently in Host mode, do nothing in device hrtimer\n");
+		return HRTIMER_NORESTART;
+	}
+
+	dsts = dwc2_readl(hsotg, DSTS);
+	dcfg = dwc2_readl(hsotg, DCFG);
+	dctl = dwc2_readl(hsotg, DCTL);
+	enum_speed = (dsts & DSTS_ENUMSPD_MASK) >> DSTS_ENUMSPD_SHIFT;
+	max_speed = (dcfg & DCFG_DEVSPD_MASK) >> DCFG_DEVSPD_SHIFT;
+
+	switch (hsotg->set_linestate) {
+	case PHY_SET_USB_NONE:
+		linestate = phy_get_linestate(hsotg->phy);
+		dev_dbg(hsotg->dev, "linestate 0x%08x dsts 0x%08x, dcfg 0x%08x\n",
+			 linestate, dsts, dcfg);
+		if ((!(dctl & DCTL_SFTDISCON)) && (max_speed == DCFG_DEVSPD_HS) &&
+		    ((enum_speed != DSTS_ENUMSPD_HS) ||
+		    ((enum_speed == DSTS_ENUMSPD_HS) &&
+		    (linestate == DP_DM_IS_HIGH)))) {
+			if (((linestate & DM_IS_HIGH) && (!hsotg->connected)) ||
+			    (hsotg->wait_enum_count >= 2)) {
+				printk_once(KERN_WARNING "usb linestate error!\n");
+				phy_set_linestate(hsotg->phy, PHY_SET_USB_DP_H_DM_L);
+				hsotg->set_linestate = PHY_SET_USB_DP_H_DM_L;
+				hsotg->wait_enum_count = 0;
+				retry = RETRY_CNT;
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(100);
+				delay = ktime_set(0, DWC2_HRTIMER_LS_RESUME);
+			} else if (!(linestate & DM_IS_HIGH) && (!hsotg->connected) &&
+				   (hsotg->gadget.speed != USB_SPEED_UNKNOWN)) {
+				/* Maybe DM Voltage in unexpected high level */
+				printk_once(KERN_WARNING "Maybe DM Volt in high level\n");
+				hsotg->wait_enum_count++;
+				delay = ktime_set(0, DWC2_HRTIMER_POLL);
+			} else {
+				delay = ktime_set(0, DWC2_HRTIMER_POLL);
+			}
+		} else {
+			delay = ktime_set(0, DWC2_HRTIMER_POLL);
+		}
+
+		break;
+	case PHY_SET_USB_DP_H_DM_L:
+		phy_set_linestate(hsotg->phy, PHY_SET_USB_DISC);
+		hsotg->set_linestate = PHY_SET_USB_DISC;
+		/*
+		 * Delay at least 15ms to wait phy hardware to complete the operation
+		 * that remove 1.5K DP pullup and enable 45ohm pulldown on DP/DM.
+		 */
+		delay = ktime_set(0, DWC2_HRTIMER_DIS);
+		break;
+	case PHY_SET_USB_DISC:
+		phy_set_linestate(hsotg->phy, PHY_SET_USB_CONNECT);
+		hsotg->set_linestate = PHY_SET_USB_CONNECT;
+		delay = ktime_set(0, wait_conn_delay);
+		break;
+	case PHY_SET_USB_CONNECT:
+		if (retry--) {
+			phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
+			linestate = phy_get_linestate(hsotg->phy);
+			if ((linestate & DP_DM_IS_HIGH)) {
+				phy_set_linestate(hsotg->phy, PHY_SET_USB_DP_H_DM_L);
+				hsotg->set_linestate = PHY_SET_USB_CONNECT;
+				delay = ktime_set(0, DWC2_HRTIMER_WAIT_SE0);
+				goto restart;
+			} else {
+				dev_dbg(hsotg->dev, "rcv se0 at retry %d delay %ld\n",
+					retry, wait_conn_delay);
+			}
+		} else {
+			dev_dbg(hsotg->dev, "retry timeout at delay %ld\n",
+				wait_conn_delay);
+
+			switch (wait_conn_delay) {
+			case DWC2_HRTIMER_FORCE_DP_H(100):
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(105);
+				break;
+			case DWC2_HRTIMER_FORCE_DP_H(105):
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(150);
+				break;
+			case DWC2_HRTIMER_FORCE_DP_H(150):
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(200);
+				break;
+			case DWC2_HRTIMER_FORCE_DP_H(200):
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(300);
+				break;
+			case DWC2_HRTIMER_FORCE_DP_H(300):
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(100);
+				break;
+			default:
+				wait_conn_delay = DWC2_HRTIMER_FORCE_DP_H(100);
+				break;
+			}
+
+			if (wait_conn_delay != DWC2_HRTIMER_FORCE_DP_H(100)) {
+				retry = RETRY_CNT;
+				phy_set_linestate(hsotg->phy, PHY_SET_USB_DP_H_DM_L);
+				hsotg->set_linestate = PHY_SET_USB_DISC;
+				dev_dbg(hsotg->dev, "retry at delay %ld\n",
+					wait_conn_delay);
+				delay = ktime_set(0, DWC2_HRTIMER_WAIT_SE0);
+				goto restart;
+			}
+		}
+
+		phy_set_linestate(hsotg->phy, PHY_SET_USB_DONE);
+		hsotg->set_linestate = PHY_SET_USB_NONE;
+		delay = ktime_set(0, DWC2_HRTIMER_POLL);
+		break;
+	default:
+		delay = ktime_set(0, DWC2_HRTIMER_POLL);
+		break;
+	}
+
+restart:
+	hrtimer_forward_now(&hsotg->check_linestate_timer, delay);
+	ret = HRTIMER_RESTART;
+	return ret;
+}
+
 /**
  * dwc2_hsotg_dump - dump state of the udc
  * @hsotg: Programming view of the DWC_otg controller
@@ -4797,6 +4961,10 @@ int dwc2_gadget_init(struct dwc2_hsotg *hsotg)
 					  epnum, 0);
 	}
 
+	hrtimer_init(&hsotg->check_linestate_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
+	hsotg->check_linestate_timer.function = &dwc2_check_linestate_timer_fn;
+	hsotg->wait_enum_count = 0;
+
 	dwc2_hsotg_dump(hsotg);
 
 	return 0;
@@ -4812,6 +4980,9 @@ int dwc2_hsotg_remove(struct dwc2_hsotg *hsotg)
 	usb_del_gadget_udc(&hsotg->gadget);
 	dwc2_hsotg_ep_free_request(&hsotg->eps_out[0]->ep, hsotg->ctrl_req);
 
+	if (soc_is_rk3308bs())
+		hrtimer_cancel(&hsotg->check_linestate_timer);
+
 	return 0;
 }
 
diff --git a/include/linux/phy/phy.h b/include/linux/phy/phy.h
index 553928a58e9f..842fa8e94df7 100644
--- a/include/linux/phy/phy.h
+++ b/include/linux/phy/phy.h
@@ -54,6 +54,14 @@ enum phy_mode {
 	PHY_MODE_DP
 };
 
+enum usb_linestate {
+	PHY_SET_USB_NONE,
+	PHY_SET_USB_DP_H_DM_L,
+	PHY_SET_USB_DISC,
+	PHY_SET_USB_CONNECT,
+	PHY_SET_USB_DONE,
+};
+
 /**
  * union phy_configure_opts - Opaque generic phy configuration
  *
@@ -117,6 +125,8 @@ struct phy_ops {
 			    union phy_configure_opts *opts);
 	int	(*reset)(struct phy *phy);
 	int	(*calibrate)(struct phy *phy);
+	int	(*get_linestate)(struct phy *phy);
+	void	(*set_linestate)(struct phy *phy, enum usb_linestate linestate);
 	struct module *owner;
 };
 
@@ -229,6 +239,8 @@ static inline enum phy_mode phy_get_mode(struct phy *phy)
 }
 int phy_reset(struct phy *phy);
 int phy_calibrate(struct phy *phy);
+int phy_get_linestate(struct phy *phy);
+void phy_set_linestate(struct phy *phy, enum usb_linestate linestate);
 static inline int phy_get_bus_width(struct phy *phy)
 {
 	return phy->attrs.bus_width;
@@ -380,6 +392,15 @@ static inline int phy_validate(struct phy *phy, enum phy_mode mode, int submode,
 	return -ENOSYS;
 }
 
+static inline void phy_set_linestate(struct phy *phy, enum usb_linestate linestate)
+{
+}
+
+static inline int phy_get_linestate(struct phy *phy)
+{
+	return -ENOSYS;
+}
+
 static inline int phy_get_bus_width(struct phy *phy)
 {
 	return -ENOSYS;
-- 
2.17.1

