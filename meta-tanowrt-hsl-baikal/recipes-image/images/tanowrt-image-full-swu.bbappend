#
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Tano Systems LLC. All rights reserved.
#
PR:append = ".baikal0"

COMPATIBLE_MACHINE = "mitx|"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/:"
